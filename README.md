# Deep Security Smart Check - Test Drive

This project has the goal to allow customer and SE's to test and try DS Smart Check integrate with Jenkinks in a DevOps pipeline process. You will be able to deploy two different web applications with differents issues.

![alt text](architecture/architecture_version1.0.png "Architecture")

# **Features**
   * Jenkins Solution to automate and monitoring DevOps pipeline
   * Deep Security Smart Check to automate the container security scanning process with Jenkins and help to protect the apps before deploy it to the production environment.

# **Deploy two web applications**
    * Web App 1 (Issues):
      - Vulnerable Apache-alpine version (2.2)
      
    * Web App 2 (Issues):
      - Malware embedded as image
      - Private Key inside the container image

# **Know Issues**
    * Automated delete process cannt delete the following resources on AWS
      - Load Balance created for the application
      - VPC's
      - ECR's
      
# **Feature Request**
    * Add SSH access through web browser
    * Add DS exercises 