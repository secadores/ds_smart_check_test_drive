#!/bin/bash
USER=administrator
ECR_BASE=`echo ${ECR_ADDRESS} | cut -d '/' -f 1`
ECR_REPO=`echo ${ECR_ADDRESS2} | cut -d '/' -f 2`

NEW_PASSWORD=${SMART_CHECK_PASSWORD}
SMART_CHECK=$(kubectl --kubeconfig=kubeconfig describe services proxy | grep Ingress | cut -d ':' -f 2 | tr -d ' ')
ORIGNAL_PASSWORD=$(kubectl --kubeconfig=kubeconfig get secrets -o jsonpath='{ .data.password }' deepsecurity-smartcheck-auth | base64 --decode)
URL=https://${SMART_CHECK}
CURL=curl
SCANURI=${URL}/api/scans
SESSIONURI=${URL}/api/sessions
CHANGEPASSWORDURI=${URL}/api/
CONTENTTYPE='context type application/json'
PERMITTEDHIGHVULNERABILITIES=0
OUT_OF_COMPLIANCE=0


change_password() {
  currentToken=`cat token`
  temp_id=`cat raw | jq .user.id`
  sed -e 's/^"//' -e 's/"$//' <<< ${temp_id} > id
  ID=`cat id`
  ${CURL} -sk -X POST ${URL}/api/users/${ID}/password -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -H "Authorization: Bearer ${TOKEN}" \
     -d '{"oldpassword":"'${ORIGNAL_PASSWORD}'","newpassword":"'${NEW_PASSWORD}'"}' > raw

}

USER_JSON='{"user": {"userid":\"${USER}\","password":\"${PASSWORD}\"}}'

#echo ${CURL} -k -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${PASSWORD}'"}}'
#${CURL} -k -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${PASSWORD}'"}}'
#${CURL} -k -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${PASSWORD}'"}}' > raw
#LOGIN_RESULT=$( ${CURL} -k -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${PASSWORD}'"}}' )
${CURL} -sk -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${ORIGNAL_PASSWORD}'"}}'  > raw
cat raw | jq .user.passwordChangeRequired >changepassword
cat raw | jq .token  > token
#${CURL} -k -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${PASSWORD}'"}}' | jq .token  > token || exit -10
TEMP_TOKEN=`cat token`
sed -e 's/^"//' -e 's/"$//' <<< ${TEMP_TOKEN} > token
TOKEN=`cat token`
#echo "My Token is: " ${TOKEN}
changepassword=`cat changepassword`
if [[ "$changepassword" == "true" ]]; then
        echo "Change passowrd is required"
        change_password
fi

#curl -fsk -X GET ${SCANURI} -H 'Content-type:application/json' -H "Authorization: Bearer ${TOKEN}" || exit -10
if [ -z ${TOKEN} ] ; then
  ${CURL} -sk -X POST ${SESSIONURI} -H 'Content-type:application/json' -H 'X-Api-Version:2018-05-01' -d '{"user": {"userid":"'${USER}'","password":"'${NEW_PASSWORD}'"}}'  > raw
  cat raw | jq .token  > token
  TEMP_TOKEN=`cat token`
  sed -e 's/^"//' -e 's/"$//' <<< ${TEMP_TOKEN} > token
  TOKEN=`cat token`
  #echo "My Token is: " ${TOKEN}
fi


${CURL} -sk -X POST ${SCANURI} -H 'Content-type:application/json' -H "Authorization: Bearer ${TOKEN}" \
    -d '{
  "id": "",
  "name": "myScan",
  "source": {
    "type": "docker",
    "registry": "'${ECR_BASE}'",
    "repository": "'${ECR_REPO}'",
    "tag": "latest",
    "credentials": {
      "token": "",
      "username": "",
      "password": "",
      "aws": {
        "region": "us-east-1",
        "accessKeyID": "",
        "secretAccessKey": "",
        "role": "",
        "externalID": "",
        "roleSessionName": "",
        "registry": ""
      }
    },
    "insecureSkipVerify": true,
    "rootCAs": ""
  }
} ' | jq .href > href || exit -10


TEMP_HREF=`cat href`
sed -e 's/^"//' -e 's/"$//' <<< ${TEMP_HREF} > href
HREF=`cat href`


${CURL} -sk -X GET ${URL}${HREF} -H 'Content-type:application/json' -H "Authorization: Bearer ${TOKEN}"  > status

TMP=`grep pending status`
while [  -n "$TMP"  ]; do
        sleep 1
        ${CURL} -sk -X GET ${URL}${HREF} -H 'Content-type:application/json' -H "Authorization: Bearer ${TOKEN}" > status
        TMP=`grep pending  status`
done
TMP=`cat status | jq .status | grep progress`
while [  -n "$TMP"  ]; do
        sleep 1
        ${CURL} -sk -X GET ${URL}${HREF} -H 'Content-type:application/json' -H "Authorization: Bearer ${TOKEN}" > status
        TMP=`cat status | jq .status | grep progress`
done

if [[ $TMP = *"completed-no-findings"* ]]; then
  echo "--------------------------------------------------------------------------------------------"
  echo "Deep Security Smart Check scan sucessful. You don't have any compliance issues!"
  echo "--------------------------------------------------------------------------------------------"
  exit 0
else

  echo "--------------------------------------------------------------------------------------------"
  echo "Deep Security Smart Check scan found some issues"
  echo "--------------------------------------------------------------------------------------------"

  ID=$(cat status | jq .id)
  if [ -z "${ID}" ] ; then
    echo "Unknown ID, please re-run Smart Check scan."
    exit -75
  fi
  MALWARE=`cat status | jq -r .findings.malware`
  if [[ $MALWARE -ne 0 ]] ; then
    echo "---> Malware detected!"
    #cat status | jq '.details.results[]'  | jq 'select(has("malware"))'
    OUT_OF_COMPLIANCE=1
    #exit -100
  fi

  CONTENT=`cat status | jq -r .findings.contents.total.high`
  #echo "Info"
  #echo "-------------"
  #echo "$CONTENT"
  #echo "-------------"
  if [[ $CONTENT -ne 0 ]]; then
    echo "--->Content Findings detected!"
    #exit -120
    OUT_OF_COMPLIANCE=1
  fi

  HIGHVULNERABILITIES=`cat status | jq -r '.findings.vulnerabilities.unresolved.high'`
  if [[ -n "$HIGHVULNERABILITIES" ]]; then
    if [[ $HIGHVULNERABILITIES -gt $PERMITTEDHIGHVULNERABILITIES ]]; then
      #echo "--------------------------------------------------------------------------------------------"
      echo "--->Number of vulerabilities found is greater than permitted High Vulnerability! "
      echo "- Total number of High Vulnerabilities found: ${HIGHVULNERABILITIES}"
      echo "- Total number of Permitted High Vulnerabilities configured: ${PERMITTEDHIGHVULNERABILITIES}"
      #echo "--------------------------------------------------------------------------------------------"
      #echo "Number of vulerabilities found is greater than permitted High Vulnerability!!!"
      #echo "--------------------------------------------------------------------------------------------"
      #echo "You are out of compliance!!!"
      #echo "--------------------------------------------------------------------------------------------"
      #cat status | jq
      #exit -150
      OUT_OF_COMPLIANCE=1
    else
      echo "--->Number of vulerabilities found is lower than permitted - High Vulnerability!!!"
      if [[ $OUT_OF_COMPLIANCE -ne 1 ]]; then
        exit 0
      fi
    fi
  fi

  if [[ $OUT_OF_COMPLIANCE -ne 0 ]]; then
    echo " "
    echo "Aborting build!"
    echo "--------------------------------------------------------------------------------------------"
    echo "YOU ARE OUT OF COMPLIANCE!!!"
    echo "--------------------------------------------------------------------------------------------"
    echo $TMP
    exit -150
  fi
  echo $TMP
  exit -128
fi
