AWSTemplateFormatVersion: 2010-09-09
Description: Lean CloudFormation for the Sec Jam.

Parameters:

    S3Bucket:
      Description: Bucket name where ALL files can be found.
      Type: String

    Domain:
      Description: Domain used for the stack.
      Type: String

    VPCCIDR:
      Description: VPC CIDR Block
      Type: String
      AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
      ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.
      Default: 10.0.0.0/16

    AdminIngressLocation:
      Description: >-
        CIDR block (IP address range) to allow SSH access to the bastion host and
        HTTPS access to the Kubernetes API. Use 0.0.0.0/0 to allow access from all
        locations.
      Type: String
      Default: 0.0.0.0/0
      MinLength: '9'
      MaxLength: '18'
      AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
      ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.

    PublicSubnet1CIDR:
      Description: >-
        CIDR Block for the Public Subnet, must be a valid subnet of the VPC CIDR.
      Type: String
      AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
      ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.
      Default: 10.0.128.0/24

    PublicSubnet2CIDR:
      Description: >-
        CIDR Block for the Public Subnet, must be a valid subnet of the VPC CIDR.
      Type: String
      AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
      ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.
      Default: 10.0.129.0/24

    AvailabilityZone1:
      Description: >-
        The Availability Zone for most everything.
      Type: 'AWS::EC2::AvailabilityZone::Name'
      ConstraintDescription: must be the name of an AWS Availability Zone

    AvailabilityZone2:
      Description: >-
        The second Availability Zone for use with EKS.
      Type: 'AWS::EC2::AvailabilityZone::Name'
      ConstraintDescription: must be the name of an AWS Availability Zone

    InstanceType:
      Description: EC2 instance type for the cluster.
      Type: String
      Default: m5.large
      AllowedValues:
        - m5.large
        - m5.xlarge
        - m5.2xlarge
        - m5.4xlarge
        - m5.12xlarge
        - m5.24xlarge
        - m5d.large
        - m5d.xlarge
        - m5d.2xlarge
        - m5d.4xlarge
        - m5d.12xlarge
        - m5d.24xlarge
        - m4.large
        - m4.xlarge
        - m4.2xlarge
        - m4.4xlarge
        - m4.10xlarge
        - m4.16xlarge
        - c5.large
        - c5.xlarge
        - c5.2xlarge
        - c5.4xlarge
        - c5.9xlarge
        - c5.18xlarge
        - t3.nano
        - t3.micro
        - t3.small
        - t3.medium
        - t3.large
        - t3.xlarge
        - t3.2xlarge
        - r5.xlarge
        - r5.2xlarge
        - r5.4xlarge
        - r5.12xlarge
        - r5.24xlarge
        - r5d.xlarge
        - r5d.2xlarge
        - r5d.4xlarge
        - r5d.12xlarge
        - r5d.24xlarge
        - r4.large
        - r4.xlarge
        - r4.2xlarge
        - r4.4xlarge
        - r4.8xlarge
        - r4.16xlarge
        - x1.16xlarge
        - x1.32xlarge
        - i3.large
        - i3.xlarge
        - i3.2xlarge
        - i3.4xlarge
        - i3.8xlarge
        - i3.16xlarge
        - c4.large
        - c4.xlarge
        - c4.2xlarge
        - c4.4xlarge
        - c4.8xlarge
        - r3.large
        - r3.xlarge
        - r3.2xlarge
        - r3.4xlarge
        - r3.8xlarge
      ConstraintDescription: must be a valid Current Generation (non-burstable) EC2 instance type.

    BastionInstanceType:
      Description: EC2 instance type for the bastion host (used for public SSH access).
      Type: String
      Default: t3.medium
      AllowedValues:
        - m5.large
        - m5.xlarge
        - m5.2xlarge
        - m5.4xlarge
        - m5.12xlarge
        - m5.24xlarge
        - m5d.large
        - m5d.xlarge
        - m5d.2xlarge
        - m5d.4xlarge
        - m5d.12xlarge
        - m5d.24xlarge
        - m4.large
        - m4.xlarge
        - m4.2xlarge
        - m4.4xlarge
        - m4.10xlarge
        - m4.16xlarge
        - c5.large
        - c5.xlarge
        - c5.2xlarge
        - c5.4xlarge
        - c5.9xlarge
        - c5.18xlarge
        - t3.nano
        - t3.micro
        - t3.small
        - t3.medium
        - t3.large
        - t3.xlarge
        - t3.2xlarge
        - r5.xlarge
        - r5.2xlarge
        - r5.4xlarge
        - r5.12xlarge
        - r5.24xlarge
        - r5d.xlarge
        - r5d.2xlarge
        - r5d.4xlarge
        - r5d.12xlarge
        - r5d.24xlarge
        - r4.large
        - r4.xlarge
        - r4.2xlarge
        - r4.4xlarge
        - r4.8xlarge
        - r4.16xlarge
        - x1.16xlarge
        - x1.32xlarge
        - i3.large
        - i3.xlarge
        - i3.2xlarge
        - i3.4xlarge
        - i3.8xlarge
        - i3.16xlarge
        - c4.large
        - c4.xlarge
        - c4.2xlarge
        - c4.4xlarge
        - c4.8xlarge
        - r3.large
        - r3.xlarge
        - r3.2xlarge
        - r3.4xlarge
        - r3.8xlarge
      ConstraintDescription: must be a valid Current Generation EC2 instance type.

    NodeInstanceType:
      Description: EC2 instance type for the node instances
      Type: String
      Default: t2.medium
      AllowedValues:
      - t2.small
      - t2.medium
      - t2.large
      - t2.xlarge
      - t2.2xlarge
      - m3.medium
      - m3.large
      - m3.xlarge
      - m3.2xlarge
      - m4.large
      - m4.xlarge
      - m4.2xlarge
      - m4.4xlarge
      - m4.10xlarge
      - m5.large
      - m5.xlarge
      - m5.2xlarge
      - m5.4xlarge
      - m5.12xlarge
      - m5.24xlarge
      - c4.large
      - c4.xlarge
      - c4.2xlarge
      - c4.4xlarge
      - c4.8xlarge
      - c5.large
      - c5.xlarge
      - c5.2xlarge
      - c5.4xlarge
      - c5.9xlarge
      - c5.18xlarge
      - i3.large
      - i3.xlarge
      - i3.2xlarge
      - i3.4xlarge
      - i3.8xlarge
      - i3.16xlarge
      - r3.xlarge
      - r3.2xlarge
      - r3.4xlarge
      - r3.8xlarge
      - r4.large
      - r4.xlarge
      - r4.2xlarge
      - r4.4xlarge
      - r4.8xlarge
      - r4.16xlarge
      - x1.16xlarge
      - x1.32xlarge
      - p2.xlarge
      - p2.8xlarge
      - p2.16xlarge
      - p3.2xlarge
      - p3.8xlarge
      - p3.16xlarge
      ConstraintDescription: must be a valid EC2 instance type

    KeyName:
      Description: Existing EC2 KeyPair for SSH access.
      Type: 'AWS::EC2::KeyPair::KeyName'
      MinLength: '1'
      MaxLength: '255'
      ConstraintDescription: Select an existing EC2 Key Pair.

    HostedZoneName:
      Description: The route53 HostedZoneName. For example, "mydomain.com."  Don't forget the period at the end.
      Type: String

    JenkinsPassword:
      Description: The password to be used by Jenkins.
      Type: String

    BastionPassword:
      Description: The password to be used by Bastion instance.
      Type: String

    SmartCheckPassword:
      Description: The password to be used by Smart Check.
      Type: String

    TeamName:
      Description: The Team's name. It is going to be used as subdomain to the hosted zone name.
      Type: String

    SmartCheckActivationCode:
      Description: Smart Check activation code.
      Type: String

    DSActivationCode:
      Description: Deep Security License key including dashes
      Type: String

    SnsDoneArn:
      Description: Topic Arn to notify when ready.
      Type: String

Resources:

    Infra:
      Type: AWS::CloudFormation::Stack
      Properties:
        TemplateURL: 
          Fn::Join: [ "", [ "https://s3.amazonaws.com/", { Ref: "S3Bucket" }, "/templates/infra.yml"]]
        Parameters:
          SnsDoneArn: !Ref SnsDoneArn
          S3Bucket: !Ref S3Bucket

    VPC:
      Type: AWS::CloudFormation::Stack
      Properties:
        TemplateURL:
          Fn::Join: [ "", [ "https://s3.amazonaws.com/", { Ref: "S3Bucket" }, "/templates/vpc.yml"]]
        Parameters:
          EnvironmentName:    !Ref AWS::StackName
          VPCCIDR:            !Ref VPCCIDR
          PublicSubnet1CIDR:  !Ref PublicSubnet1CIDR
          PublicSubnet2CIDR:  !Ref PublicSubnet2CIDR
          AdminIngressLocation: !Ref AdminIngressLocation
          AvailabilityZone1:  !Ref AvailabilityZone1
          AvailabilityZone2:  !Ref AvailabilityZone2

    DSM:
      Type: AWS::CloudFormation::Stack
      Properties:
        Parameters:
          InstanceType: !Ref InstanceType
          KeyName: !Ref KeyName
          VPC: !GetAtt VPC.Outputs.VPC
          PublicSubnet: !GetAtt VPC.Outputs.PublicSubnet1
          PublicSubnetCIDR:  !Ref PublicSubnet1CIDR
          AdminIngressLocation: !Ref AdminIngressLocation
          ReadS3BucketProfile: !GetAtt Infra.Outputs.ReadS3BucketProfile
          DSActivationCode: !Ref DSActivationCode
          S3Bucket: !Ref S3Bucket
        Tags:
          - Key: Name
            Value: dsm-host-stack
        TemplateURL:
          Fn::Join: [ "", [ "https://s3.amazonaws.com/", { Ref: "S3Bucket" }, "/templates/dsm.yml"]]
        TimeoutInMinutes: 30

    Jenkins:
      Type: AWS::CloudFormation::Stack
      Properties:
        Parameters:
          InstanceType: !Ref InstanceType
          KeyName: !Ref KeyName
          VPC: !GetAtt VPC.Outputs.VPC
          PublicSubnet: !GetAtt VPC.Outputs.PublicSubnet1
          PublicSubnetCIDR:  !Ref PublicSubnet1CIDR
          AdminIngressLocation: !Ref AdminIngressLocation
          DsmDns: !GetAtt DSM.Outputs.DsmDns
          ReadS3BucketProfile: !GetAtt Infra.Outputs.ReadS3BucketProfile
          GitURL: !GetAtt Infra.Outputs.GitHTTPSUrl
          EcrRepositoryArn: !GetAtt Infra.Outputs.EcrRepositoryArn
          EcrRepositoryAddress: !GetAtt Infra.Outputs.EcrRepositoryAddress
          GitURL2: !GetAtt Infra.Outputs.GitHTTPSUrl2
          EcrRepositoryArn2: !GetAtt Infra.Outputs.EcrRepositoryArn2
          EcrRepositoryAddress2: !GetAtt Infra.Outputs.EcrRepositoryAddress2
          HostedZoneName: !Ref HostedZoneName
          JenkinsPassword: !Ref JenkinsPassword
          SmartCheckPassword: !Ref SmartCheckPassword
          TeamName: !Ref TeamName
          S3Bucket: !Ref S3Bucket
        Tags:
          - Key: Name
            Value: jenkins-host-stack
        TemplateURL: 
          Fn::Join: [ "", [ "https://s3.amazonaws.com/", { Ref: "S3Bucket" }, "/templates/jenkins.yml"]]
        TimeoutInMinutes: 30

    Bastion:
      Type: AWS::CloudFormation::Stack
      Properties:
        Parameters:
          BastionInstanceType: !Ref BastionInstanceType
          KeyName: !Ref KeyName
          VPC: !GetAtt VPC.Outputs.VPC
          PublicSubnet1: !GetAtt VPC.Outputs.PublicSubnet1
          PublicSubnet2: !GetAtt VPC.Outputs.PublicSubnet2
          AdminIngressLocation: !Ref AdminIngressLocation
          DsmDns: !GetAtt DSM.Outputs.DsmDns
          ReadS3BucketProfile: !GetAtt Infra.Outputs.ReadS3BucketProfile
          IamRole: !GetAtt Infra.Outputs.IamRole
          GitURL: !GetAtt Infra.Outputs.GitHTTPSUrl
          EcrRepositoryArn: !GetAtt Infra.Outputs.EcrRepositoryArn
          EcrRepositoryAddress: !GetAtt Infra.Outputs.EcrRepositoryAddress
          GitURL2: !GetAtt Infra.Outputs.GitHTTPSUrl2
          EcrRepositoryArn2: !GetAtt Infra.Outputs.EcrRepositoryArn2
          EcrRepositoryAddress2: !GetAtt Infra.Outputs.EcrRepositoryAddress2
          HostedZoneName: !Ref HostedZoneName
          BastionPassword: !Ref BastionPassword
          SmartCheckPassword: !Ref SmartCheckPassword
          JenkinsPassword: !Ref JenkinsPassword
          TeamName: !Ref TeamName
          NodeInstanceType: !Ref NodeInstanceType
          SmartCheckActivationCode: !Ref SmartCheckActivationCode
          SnsDoneArn: !Ref SnsDoneArn
          S3Bucket: !Ref S3Bucket
          Domain: !Ref Domain
        Tags:
          - Key: Name
            Value: bastion-host-stack
        TemplateURL:
          Fn::Join: [ "", [ "https://s3.amazonaws.com/", { Ref: "S3Bucket" }, "/templates/bastion.yml"]]
        TimeoutInMinutes: 30

Outputs:

    CompanyName:
      Description: Companie's Name
      Value: !Ref TeamName

    BastionAccess:
      Description: Bastion Public username and DNS name to SSH access.
      Value: !GetAtt Bastion.Outputs.BastionAccess

    DSMUrl:
      Description: Deep Security Manager URL
      Value: !GetAtt DSM.Outputs.DSMUrl

    JenkinsUrl:
      Description: Jenkins URL
      Value: !GetAtt Jenkins.Outputs.JenkinsUrl

    GitUrl:
      Description: The URL to use for cloning the repository over SSH, such as ssh://git-codecommit.us-east-1.amazonaws.com/v1/repos/MyDemoRepo.
      Value: !GetAtt Infra.Outputs.GitUrl

    GitHTTPSUrl:
      Description: The HTTPS url of the git repo
      Value: !GetAtt Infra.Outputs.GitHTTPSUrl

    GitUrl2:
      Description: The URL to use for cloning the repository over SSH, such as ssh://git-codecommit.us-east-1.amazonaws.com/v1/repos/MyDemoRepo.
      Value: !GetAtt Infra.Outputs.GitUrl2

    GitHTTPSUrl2:
      Description: The HTTPS url of the git repo
      Value: !GetAtt Infra.Outputs.GitHTTPSUrl2
