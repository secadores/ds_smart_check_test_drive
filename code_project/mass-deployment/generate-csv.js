const Json2csvParser = require('json2csv').Parser;
const crypto = require('crypto');

const fields = ['jenkinsUrl', 'jenkinsUsername', 'jenkinsPassword', 'bastionFqdn', 'bastionUsername', 'bastionPassword'];

var numberOfTeams, hostedZoneName;
// If the provided Arguments are not right
if (process.argv.length <= 2){
  console.log('use: number-of-teams hosted-zone-name')
  return -1
}
else{
  numberOfTeams = process.argv[2];
  hostedZoneName = process.argv[3];
}

var teams = [];
for (var i = 0; i < numberOfTeams; i++){
  j = i + 1;
  var team = {
    'jenkinsUrl' : 'jenkins.team'+ j + '.' + hostedZoneName,
    'jenkinsUsername' : 'admin',
    'jenkinsPassword' : crypto.randomBytes(15).toString('hex'),
    'bastionFqdn' : 'bastion.team'+ j + '.' + hostedZoneName,
    'bastionUsername' : 'ec2-user',
    'bastionPassword' : crypto.randomBytes(15).toString('hex')
  };
  teams.push(team);
};

const json2csvParser = new Json2csvParser({ fields, quote: '' });
const csv = json2csvParser.parse(teams);

console.log(csv);
