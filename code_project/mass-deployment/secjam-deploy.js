'use strict';

var AWS = require('aws-sdk');
var fs = require('fs');
var csv = require("csvtojson");

// global variables
var region, filePath, keyName, smartCheckActivationCode, dsActivationCode;
keyName = 'rbottino';

// If the provided Arguments are not right
if (process.argv.length <= 2){
  console.log('use: aws-region csv-file-path')
  return -1
}
else{
  region = process.argv[2];
  filePath = process.argv[3];
  smartCheckActivationCode = 'DK-REP9-GTEWW-2G9LY-W8KA6-9GK6K-H2CUC'
  dsActivationCode = 'DX-M2FZ-H6S7T-64CUT-2N9ND-SSG48-93U3Q'
}
AWS.config.update({region: region});
var cloudformation = new AWS.CloudFormation();
var start = new Date();



var prepareParams = function(team, region, hostedZoneName, keyName, bastionPassword, jenkinsPassword, smartCheckPassword){
  var params = {
    StackName: 'SecJam-' + team,
    Capabilities: [
      'CAPABILITY_IAM'
    ],
    Parameters: [
      {
        ParameterKey: 'AvailabilityZone1',
        ParameterValue: region + 'a'
      },
      {
        ParameterKey: 'AvailabilityZone2',
        ParameterValue: region + 'b'
      },
      {
        ParameterKey: 'BastionPassword',
        ParameterValue: bastionPassword
      },
      {
        ParameterKey: 'HostedZoneName',
        ParameterValue: hostedZoneName
      },
      {
        ParameterKey: 'JenkinsPassword',
        ParameterValue: jenkinsPassword
      },
      {
        ParameterKey: 'SmartCheckPassword',
        ParameterValue: smartCheckPassword
      },
      {
        ParameterKey: 'TeamName',
        ParameterValue: team
      },{
        ParameterKey: 'KeyName',
        ParameterValue: keyName
      },
      {
        ParameterKey: 'SmartCheckActivationCode',
        ParameterValue: smartCheckActivationCode
      },
      {
        ParameterKey: 'DSActivationCode',
        ParameterValue: dsActivationCode
      }
    ],
    Tags: [
      {
        Key: 'SecJam', /* required */
        Value: 'Yes' /* required */
      },
      {
        Key: 'Team', /* required */
        Value: team /* required */
      }
    ],
    TemplateURL: 'https://s3.amazonaws.com/dev.smartcheckdemo/lean-formation.yml',
    TimeoutInMinutes: 60
  };
  return params;
};

var createTeam = async function(team, region, hostedZoneName, keyName, bastionPassword, jenkinsPassword, smartCheckPassword){
  var params = prepareParams(team, region, hostedZoneName, keyName, bastionPassword, jenkinsPassword, smartCheckPassword);
  return await Promise.resolve(cloudformation.createStack(params).promise());
};

var checkTeamCreation = async function(id){
  try{
    return await Promise.resolve(cloudformation.waitFor('stackCreateComplete', {}).promise());
  }
  catch(err){
    return id;
  };
};

var parseCsv = async function(filePath){
  try{
    var jsonArrayObj = await csv().fromFile(filePath);
    return jsonArrayObj;
  }
  catch(err){
    return err;
  };
};

var redeployStacksifNeeded = async function(teams){
  try{
    var res = await Promise.resolve(cloudformation.listStacks({'StackStatusFilter': ['CREATE_COMPLETE', 'DELETE_FAILED']}).promise());
    //console.log(res);
    var retry = [];
    for (var i = 0; i < res.StackSummaries.length; i++){
      var stackName = res.StackSummaries[i].StackName;
      var regex = RegExp('/SecJam-team(\d){1,3}/g');
      if (regex.test(stackName)){
        let j = stackName.split('-')[1]; // team number
        retry.push(teams[j-1]);
      }
      var data = await createTeams(retry);
    };
    return retry;
  }
  catch(err){
    return err
  };
}

var createTeams = async function(teams){
  var hostedZoneNameArray = teams[0].jenkinsUrl.split('.');
  var hostedZoneName = hostedZoneNameArray.splice(0,2);
  hostedZoneName = hostedZoneNameArray.join('.').concat('.');;
  var stacksIds = [];
  for (var i = 1; i < teams.length+1; i++){
    var team = 'team' + i;
    var bastionPassword = teams[i-1].bastionPassword;
    var jenkinsPassword =  teams[i-1].jenkinsPassword;
    var smartCheckPassword = 'password';
    var data = await createTeam(team, region, hostedZoneName, keyName, bastionPassword, jenkinsPassword, smartCheckPassword);
    // wait 5 seconds
    await new Promise((resolve, reject) => setTimeout(resolve, 5000));
    stacksIds.push(data.StackId);
  };
  console.log(stacksIds.length);
  var retry = []
  for (var i =0; i < stacksIds.length; i++){
    var res = await checkTeamCreation(stacksIds[i])
    console.log(res);
    if (res == stacksIds[i])
      retry.push(i);
  };
  for (var i = 0; i < retry.length; i++){
    var team = 'team' + retry[i];
    var bastionPassword = retry[i].bastionPassword;
    var jenkinsPassword =  retry[i].jenkinsPassword;
    var smartCheckPassword = 'password';
    var data = await createTeam(team, region, hostedZoneName, keyName, bastionPassword, jenkinsPassword, smartCheckPassword);
    stacksIds.push(data.StackId);
  };
  var end = new Date() - start;
  console.log('Execution time: ', end);
  return stacksIds;
};


parseCsv(filePath)
  .then(teams => {
    var stacksIds = createTeams(teams);
  })
  .catch(err => {
    console.log(err);
  });
