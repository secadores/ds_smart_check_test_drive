'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');
const crypto = require('crypto');
const Grecaptcha = require('grecaptcha')

const recaptcha = new Grecaptcha('6LdxDZMUAAAAADBgl6ORlEGs_gTxzGNCQPc60JfH')

const dynamoDb = new AWS.DynamoDB.DocumentClient();

var generatePassword = function(){
  return crypto.randomBytes(15).toString('hex');
}

var returnHandler = async function(status, message){
  let output = {
    "isBase64Encoded": false,
    "statusCode": status,
    "headers": {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": "true"
    },
    "body": JSON.stringify(message)
  };
  return output;
}

var createStack = async function(company){
  var cloudformation = new AWS.CloudFormation();
  var params = {
    StackName: "demo-" + company,
    // NotificationARNs: [
    //   process.env.SNS_TOPIC_ARN
    // ],
    Capabilities: [
      'CAPABILITY_IAM'
    ],
    Parameters: [
      {
        ParameterKey: 'S3Bucket',
        ParameterValue: process.env.S3_BUCKET
      },
      {
        ParameterKey: 'Domain',
        ParameterValue: process.env.DEMO_HZ
      },
      {
        ParameterKey: 'AdminIngressLocation',
        ParameterValue: '0.0.0.0/0'
      },
      {
        ParameterKey: 'AvailabilityZone1',
        ParameterValue: 'us-east-1a'
      },
      {
        ParameterKey: 'AvailabilityZone2',
        ParameterValue: 'us-east-1b'
      },
      {
        ParameterKey: 'BastionInstanceType',
        ParameterValue: 't3.medium'
      },
      {
        ParameterKey: 'BastionPassword',
        ParameterValue: generatePassword()
      },
      {
        ParameterKey: 'DSActivationCode',
        ParameterValue: 'AP-28AS-AT9GT-QHVJA-F4WUU-R59YG-7KWUC'
      },
      {
        ParameterKey: 'HostedZoneName',
        ParameterValue: process.env.DEMO_HZ + '.'
      },
      {
        ParameterKey: 'InstanceType',
        ParameterValue: 'm5.large'
      },
      {
        ParameterKey: 'JenkinsPassword',
        ParameterValue: generatePassword()
      },
      {
        ParameterKey: 'KeyName',
        ParameterValue: 'workshop_key'
      },
      {
        ParameterKey: 'NodeInstanceType',
        ParameterValue: 't2.medium'
      },
      {
        ParameterKey: 'PublicSubnet1CIDR',
        ParameterValue: '10.0.128.0/24'
      },
      {
        ParameterKey: 'PublicSubnet2CIDR',
        ParameterValue: '10.0.129.0/24'
      },
      {
        ParameterKey: 'SmartCheckActivationCode',
        ParameterValue: 'AP-R9RM-92WHD-B2UR5-BV2YB-HZYM8-HYYVA'
      },
      {
        ParameterKey: 'SmartCheckPassword',
        ParameterValue: generatePassword()
      },
      {
        ParameterKey: 'TeamName',
        ParameterValue: company
      },
      {
        ParameterKey: 'VPCCIDR',
        ParameterValue: '10.0.0.0/16'
      },
      {
        ParameterKey: 'SnsDoneArn',
        ParameterValue: process.env.SNS_TOPIC_ARN
      }
    ],
    Tags: [
      {
        Key: 'Demo', /* required */
        Value: 'Yes' /* required */
      },
      /* more items */
    ],
    TemplateURL: 'https://s3.amazonaws.com/' + process.env.S3_BUCKET + '/lean-formation.yml',
    TimeoutInMinutes: 20
  };
  try {
    let data = await cloudformation.createStack(params).promise();
    console.log('success');
    console.log(data);
    return await returnHandler(200, {"success": "true"});
  }
  catch (err){
    console.log('err');
    console.log(err);
    return await returnHandler(501, {"success": "false"});
  }
}

module.exports.post = async (event, context) => {
  const timestamp = new Date().getTime();
  console.log(event.body);
  const data = JSON.parse(event.body);
  console.log(data);
  if (typeof data.name !== 'string') {
    console.error('Validation Failed');
    return returnHandler(400, {"success": "false"});
  }

  const grecaptcha = data.grecaptcha;
  let id = uuid.v1();
  let stackName = data.company.split(' ').join('-') + "-" + id;
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      id: id,
      name: data.name,
      email: data.email,
      company: data.company,
      created: false,
      createdAt: timestamp,
      updatedAt: timestamp,
      stackName: stackName
    },
  };

  if (await recaptcha.verify(grecaptcha)) {
    console.log(params)
    try{
      // write the todo to the database
      const data = await dynamoDb.put(params).promise();
      console.log(data);
      return await createStack(stackName);
    }
    catch (error){
      console.error(error);
      return await returnHandler(501, {"success": "false"});
    };
  };
}
