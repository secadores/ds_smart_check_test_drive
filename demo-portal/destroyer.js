'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient();
const cloudformation = new AWS.CloudFormation();

const MY_EMAIL = "raphael_bottino@trendmicro.com";
const EMAIL_SOURCE = "smartcheck@" + process.env.DEMO_HZ;
const HOURS_LIMIT = 1000*3600*2   ;//10; //2hours
const STACK_PREFIX = 'demo-'

var getEntries = async function(){
  let timelimit = new Date().getTime();
  timelimit = timelimit - HOURS_LIMIT;
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    FilterExpression: "created = :created AND updatedAt < :timelimit",
    ExpressionAttributeValues: {
        ":created": true,
        ":timelimit": timelimit
    }
  };
  console.log(params);
  try {
    let data = await ddb.scan(params).promise();
    console.log(data.Items);
    return data.Items;
  }
  catch (err){
    console.log(err);
    return err;
  }
};

var getStack = async function(arn){
  var params = {
    StackName: arn
  };
  try{
    let data = await cloudformation.describeStacks(params).promise();
    console.log(data);
    return data.Stacks[0];
  }
  catch(err){
    console.log(err);
    return err;
  }

};

var deleteStack = async function(stackName){
  var params = {
    StackName: stackName
  };
  try {
    let data = await cloudformation.deleteStack(params).promise();
    console.log(data);
    return data;
  }
  catch(err){
    console.log(err);
    return err;
  }
};

var updateEntry = async function(id){
  const timestamp = new Date().getTime();
  try{
    var parms = {
      TableName: process.env.DYNAMODB_TABLE,
      Key: { "id" : id  },
      ExpressionAttributeValues:
        {
          ':updatedAt_new' : timestamp,
          ':created_new' : "done"
        },
      UpdateExpression:"set updatedAt = :updatedAt_new, created = :created_new",
      ReturnValues: "UPDATED_NEW"
    };
    let data = await ddb.update(parms).promise();
    console.log(data);
    return data;
  }
  catch (error) {
    console.log(error);
    return false;
  }
};

var sendEmail = async function(entry){
  var params = {
    Destination: { /* required */
      CcAddresses: [
        MY_EMAIL
      ],
      ToAddresses: [
        entry.email
      ]
    },
    Message: { /* required */
      Body: { /* required */
        Text: {
         Charset: "UTF-8",
         Data: "Hello, " + entry.name + ". This email notifies you that your demo period for the Smart Check demo is over. We hope you had a great experience!"
        }
       },
       Subject: {
        Charset: 'UTF-8',
        Data: 'Your Smart Check Demo is Done. How was it?'
       }
      },
    Source: EMAIL_SOURCE,
    ReplyToAddresses: [
       MY_EMAIL
    ]
  };
  let data = await new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
  console.log(data);
  return data;
};

module.exports.handler = async (event, context) => {
  console.log(event);
  let entries = await getEntries();
  console.log(entries);
  for (let i = 0; i < entries.length; i++){
    try{
      const data = await getStack(entries[i].eksArn);
      console.log(data);
      console.log(data.StackName);
      await deleteStack(data.StackName);
      await deleteStack(STACK_PREFIX + entries[i].stackName);
      await updateEntry(entries[i].id);
      return await sendEmail(entries[i]);
    }
    catch(err){
      console.log(err);
      return err;
    }
  }
  return entries;
}
