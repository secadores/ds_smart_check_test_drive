'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient();
const MY_EMAIL = "raphael_bottino@trendmicro.com";
const EMAIL_SOURCE = "smartcheck@" + process.env.DEMO_HZ;

var parseSns = function(event){
  var messages = [];
  (event.Records || []).forEach(function(record) {
    if (record.Sns) {
      var parsed = {};
      var text = '';
      try {
        parsed = JSON.parse(record.Sns.Message);
        messages.push(parsed);
      }
      catch(e) {
        text = record.Sns.Message;
        messages.push(text);
      }
    }
  });
  if (messages.length == 1) {
    return messages[0];
  }
  else {
    return messages;
  }
};

var getEntry = async function(stackName){
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    FilterExpression: "stackName = :stackName",
    ExpressionAttributeValues: {
        ":stackName": stackName
    }
  };
  console.log(params);
  try {
    let data = await ddb.scan(params).promise();
    console.log(data.Items);
    return data.Items[0];
  }
  catch (err){
    console.log(err);
    return err;
  }
};

var updateEntry = async function(entry, eksArn){
  const timestamp = new Date().getTime();

  try{
    var parms = {
      TableName: process.env.DYNAMODB_TABLE,
      Key: { "id" : entry.id  },
      ExpressionAttributeValues:
        {
          ':updatedAt_new' : timestamp,
          ':created_new' : true,
          ':eksArn_new' : eksArn
        },
      //ConditionExpression: "updatedAt = :updatedAt_orignal AND created = :created_original",
      UpdateExpression:"set updatedAt = :updatedAt_new, created = :created_new, eksArn = :eksArn_new",
      ReturnValues: "UPDATED_NEW"
    };
    let data = await ddb.update(parms).promise();
    console.log(data);
    return data;
  }
  catch (error) {
    console.log(error);
    return false;
  }
};

var sendEmail = async function(email, message){
  var params = {
    Destination: { /* required */
      CcAddresses: [
        MY_EMAIL
      ],
      ToAddresses: [
        email
      ]
    },
    Message: { /* required */
      Body: { /* required */
        Text: {
         Charset: "UTF-8",
         Data: "The Smart Check URL is: " + message.smartCheckUrl + "\nThe username is: " + message.smartCheckUsername + "\nThe password is: "  + message.smartCheckPassword + "\nThe Jenkins url is: " + message.jenkinsUrl + "\nThe username is: " + message.jenkinsUsername + "\nThe password is: " + message.jenkinsPassword + "\nSSH command: ssh " + message.sshUsername + "@" + message.sshUrl + "\nSSH DNS is: " + message.sshUrl + "\nThe username is: " + message.sshUsername + "\nThe password is: " + message.sshPassword
        }
       },
       Subject: {
        Charset: 'UTF-8',
        Data: 'Your Smart Check Demo is Ready!'
       }
      },
    Source: EMAIL_SOURCE,
    ReplyToAddresses: [
       MY_EMAIL
    ]
  };
  let data = await new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
  console.log(data);
  return data;
};

module.exports.handler = async (event, context) => {
  let message = parseSns(event);
  console.log(message);
  let entry = await getEntry(message.companyName);
  console.log(entry);
  let email = entry.email;
  let updatedEntry = await updateEntry(entry, message.eksArn);
  console.log(updatedEntry);
  return sendEmail(email, message);
}
