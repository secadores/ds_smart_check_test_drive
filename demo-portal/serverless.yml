service: demo-portal

provider:
  name: aws
  runtime: nodejs12.x
  memorySize: 128
  environment:
    DYNAMODB_TABLE: ${self:service}-${opt:stage, self:provider.stage}
    S3_BUCKET: ${env:S3_BUCKET}
    DEMO_URL: ${env:DEMO_URL}
    DEMO_HZ: ${env:DEMO_HZ}
  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.DYNAMODB_TABLE}"
    - Effect: Allow
      Action:
        - cloudformation:CreateStack
        - cloudformation:DescribeStacks
        - cloudformation:ListStacks
        - cloudformation:DeleteStack
      Resource: "*"
    - Effect: Allow
      Action:
        - SNS:Publish
      Resource: { "Fn::Join" : [":", ["arn:aws:sns:${self:custom.region}", { "Ref" : "AWS::AccountId" }, "${self:custom.snsTopic}" ] ]  }
    - Effect: Allow
      Action:
        - ec2:*
      Resource: "*"
    - Effect: Allow
      Action:
        - ses:*
      Resource: "*"
    - Effect: Allow
      Action:
        - s3:*
      Resource:
        - "arn:aws:s3:::${env:S3_BUCKET}"
        - "arn:aws:s3:::${env:S3_BUCKET}/*"
    - Effect: Allow
      Action:
        - ecr:*
      Resource: "*"
    - Effect: Allow
      Action:
        - iam:*
      Resource: "*"
    - Effect: Allow
      Action:
        - codecommit:*
      Resource: "*"
    - Effect: Allow
      Action:
        - route53:*
      Resource: "*"
    - Effect: Allow
      Action:
        - eks:*
      Resource: "*"
    - Effect: Allow
      Action:
        - autoscaling:*
      Resource: "*"


plugins:
  - serverless-s3-sync

custom:
  hostedZoneName: ${env:DEMO_HZ}
  siteName: ${env:DEMO_URL}
  aliasHostedZoneId: Z3AQBSTGFYJSTF    # us-east-1
  aliasDNSName: s3-website-us-east-1.amazonaws.com
  s3Sync:
    - bucketName: ${env:DEMO_URL}
      localDir: static
  stage: ${opt:stage, self:provider.stage}
  region: ${opt:region, self:provider.region}
  snsTopic: "created-stacks-${self:custom.stage}"

resources:
  Resources:

    StaticSite:
      Type: AWS::S3::Bucket
      Properties:
        AccessControl: PublicRead
        BucketName: ${self:custom.siteName}
        WebsiteConfiguration:
          IndexDocument: index.html
          ErrorDocument: index.html
        # Set the CORS policy
        CorsConfiguration:
          CorsRules:
            -
              AllowedOrigins:
                - '*'
              AllowedHeaders:
                - '*'
              AllowedMethods:
                - GET
                - PUT
                - POST
                - DELETE
                - HEAD
              MaxAge: 3000

    StaticSiteS3BucketPolicy:
      Type: AWS::S3::BucketPolicy
      Properties:
        Bucket:
          Ref: StaticSite
        PolicyDocument:
          Statement:
            - Sid: PublicReadGetObject
              Effect: Allow
              Principal: '*'
              Action:
                - s3:GetObject
              Resource:
                Fn::Join: [
                  "", [
                    "arn:aws:s3:::",
                    {
                      "Ref": "StaticSite"
                    },
                    "/*"
                  ]
                ]

    DnsRecord:
      Type: "AWS::Route53::RecordSet"
      Properties:
        AliasTarget:
          DNSName: ${self:custom.aliasDNSName}
          HostedZoneId: ${self:custom.aliasHostedZoneId}
        HostedZoneName: ${self:custom.hostedZoneName}.
        Name: ${self:custom.siteName}
        Type: 'A'

    QueueDynamoDbTable:
      Type: 'AWS::DynamoDB::Table'
      DeletionPolicy: Retain
      Properties:
        AttributeDefinitions:
          -
            AttributeName: id
            AttributeType: S
        KeySchema:
          -
            AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        TableName: ${self:provider.environment.DYNAMODB_TABLE}


functions:
  build-stack:
    handler: build-stack.post
    events:
      - http:
          path: check
          method: post
          cors: true
    environment:
      SNS_TOPIC_ARN: { "Fn::Join" : [":", ["arn:aws:sns:${self:custom.region}", { "Ref" : "AWS::AccountId" }, "${self:custom.snsTopic}" ] ]  }

  notifier:
    handler: notifier.handler
    events:
      - sns: ${self:custom.snsTopic}
        filterPolicy:
          ResourceStatus:
            - CREATE_COMPLETE

  destroyer:
    handler: destroyer.handler
    events:
      - schedule: rate(10 minutes)
